#!/bin/bash

rm -rf ./graph

mkdir ./graph

docker run -t -v $(pwd)/domain.txt:/opt/kaldi/model/db/domain.txt -v $(pwd)/graph:/opt/kaldi/model/exp/tdnn/graph veptechno/asr-model-builder:1.0

docker build -t veptechno/asr-server:1.0 .
