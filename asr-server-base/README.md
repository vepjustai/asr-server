# Базовый образ для asr сервиса

Здесь представлены файлы и директории для сборки базового образа запускающего в себе сервер распознающий голос на основе
предсобранной модели распознавания.

_Этот образ не требует пересобирания, в идеале сборка производится один раз. Однако если вносятся изменения в код
сервера, то вам придётся его пересобрать._

## Как собрать образ

Запустите скрипт _build.sh_. **Учтите, что в скрипте указан тег образа, который вам желательно заменить на свой**

## Как запустить образ

Этого делать не рекомендуется, так как это базовый образ не предполагающий запуска.