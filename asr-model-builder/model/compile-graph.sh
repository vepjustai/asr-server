#!/bin/bash

cd "$(dirname "$0")"
. path.sh

rm -rf data
rm -rf exp/lgraph

mkdir -p data/dict
cp db/phone/* data/dict

export PYTHONIOENCODING=utf8

echo "Creating language model..."
custom.py db/ru.dic db/domain.txt > data/custom.dic
dictionary.py data/custom.dic > data/ru.dic
cat db/ru.dic >> data/ru.dic

#cat db/domain.txt db/text.txt > data/text.txt
cp db/domain.txt data/text.txt

#dict.py data/text.txt data/ru.dic > data/dict/lexicon.txt

ngram-count -text data/text.txt -lm data/text.lm.gz -cdiscount 0.1

echo "Merging..."
ngram -lm data/text.lm.gz -mix-lm db/ru-small.lm.gz -lambda 0.5 -write-lm data/final.lm.gz

sh dict_prep.sh

echo "Preparing language..."
utils/prepare_lang.sh data/dict "[unk]" data/local_lang data/lang

echo "Formatting..."
utils/format_lm.sh data/lang data/final.lm.gz data/dict/lexicon.txt data/lang_test

#utils/mkgraph_lookahead.sh \
#          --self-loop-scale 1.0 data/lang \
#          exp/tdnn data/text.lm.gz exp/tdnn/lgraph

echo "Compiling graph..."
utils/mkgraph.sh --self-loop-scale 1.0 data/lang_test exp/tdnn exp/tdnn/graph

echo "Done!"
rm -rf data
