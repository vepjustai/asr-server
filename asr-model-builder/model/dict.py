#!/usr/bin/python3

import sys

words = set()

for line in open(sys.argv[1], encoding='utf-8'):
    for w in line.split():
        words.add(w)

for line in open(sys.argv[2], encoding='utf-8'):
    line = line.strip()
    items = line.split()
    if items[0] in words:
        print (line)

print ("!SIL SIL")
print ("[unk] GBG")
